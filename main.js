function isAscii(str) {
    return !/[^\x00-\x7f]/.test(str);
}

let text = document.getElementById('content');
let character_count = document.getElementById("character-count");
let symbol=0;
text.onkeyup = function(evt) {
    let str = this.value;
    let count = str.length;
    if (isAscii(str)) {
        if (count >= 140) {
            symbol+=count;
            //console.log("count ascii", count);
            text.value="";
        }
    } else{
        if (count >=70) {
           symbol+=count;
           //console.log("count utf8", count);
           text.value="";
        }
    }
    character_count.innerHTML = 'Текущее количество символов: ' + count + ". Общее количество символов "+ symbol;
}